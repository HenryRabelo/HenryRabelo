## CyberSec Admin & Tech Enthusiast
[![Fedora Badge](https://img.shields.io/badge/Fedora-51A2DA?logo=fedora&logoColor=white)](https://fedoraproject.org/)
[![Chromium Badge](https://img.shields.io/badge/Chromium-4285F4?logo=googlechrome&logoColor=white)](https://chromium.org/Home)
[![ZAP Badge](https://img.shields.io/badge/ZAP-00549E?logo=zap)](https://zaproxy.org/)
[![VSCodium Badge](https://img.shields.io/badge/VSCodium-2F80ED?logo=vscodium&logoColor=white)](https://vscodium.com/)
[![Android Studio Badge](https://img.shields.io/badge/Android_Studio-34A853?logo=androidstudio&logoColor=white)](https://developer.android.com/studio)
[![GitLab Badge](https://img.shields.io/badge/GitLab-FC6D26?logo=gitlab&logoColor=white)](https://gitlab.com/)
[![Podman Badge](https://img.shields.io/badge/Podman-892CA0?logo=podman)](https://podman.io/)
[![Open Source Badge](https://img.shields.io/badge/Open_Source-3DA639?logo=opensourceinitiative&logoColor=white)](https://opensource.org/)

<img src="https://gitlab.com/henryrabelo/henryrabelo/-/raw/main/assets/imgs/workstation.png" alt="Workstation" width="350" height="350" align="right"/>

<p align="left">

  Hi there! Thank you for checking out my GitLab! ✨

  Sometimes I upload personal projects and scripts, things I might use or just random things.

  I like researching security and privacy concepts, and what can be done to better secure a system. There is no perfectly secure system, but I strive to have a good balance between Security, Privacy & Convenience.

</p>

#### My workflow 💻
I try to harness the best of the open source tools in my field and I try to always be practical when it comes to my choices. I've used just about every mainstream OS, but I tend to champion Linux systems, because of their flexibility and ease of use.

I use containers that have the main tools of my workflow installed, so I can have a highly reproducible and clean work environment. Security best practices such as 2FA are a must, and compartmentalization of the tools I use further secures my environment.

#### Languages I speak:
- English
- Portuguese
- I am currently learning German! 🇩🇪
  - Ich spreche nur ein bisschen jetzt, aber Ich lerne schnell! Ich finde Ich lerne gern deustch. 🤓

#### Some relevant courses 🎓
###### Networking:
- ![Cisco CyberOps Badge](https://img.shields.io/badge/Cisco_CyberOps_Associate-1BA0D7?logo=cisco&logoColor=white)
- ![Cisco CCNA Badge](https://img.shields.io/badge/Cisco_CCNA_I-1BA0D7?logo=cisco&logoColor=white)
###### System administration:
- ![RH Admin I Badge](https://img.shields.io/badge/Red_Hat_System_Administration_I_(RH124)-EE0000?logo=redhat)
- ![RH Admin II Badge](https://img.shields.io/badge/Red_Hat_System_Administration_II_(RH134)-EE0000?logo=redhat)


##
###### URL Hash (SHA512):
```
ac3931407a95a3b77caf4129b1481cbfa255656709fb10702c77d52fe2e1a93b983c97dfb75312e4415fde6bc630ca417778c830126c33d61bb701033c9ab749
```
